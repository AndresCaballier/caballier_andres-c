/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "timer.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <UART.h>
#include "../inc/Llamado2_Ejercicio2.h"       /* <= own header */
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
/*Diseñe e implemente un firmware que mida el voltaje generado
 * por un sensor de presión conectado a la entrada CH1 de la EDU-CIAA a una frecuencia de muestreo de 25 Hz.
 *  Los datos se procesarán y enviarán por el puerto serie a la pc en un formato de caracteres legible por consola (uno por renglón).
 * El procesamiento a realizar se elige utilizando las teclas de la EDU-CIAA:

Tecla 1: valor promedio del último segundo de datos
Tecla 2: valor máximo del último segundo de datos
Tecla 3: valor mínimo del último segundo de datos
 *

 *
 *
 *
 *
 *
 *
 *
 * */
/*==================[internal data definition]===============================*/
#define VALOR25CUENTAS 25

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
#define basediez 10

uint16_t caracter;
uint16_t senial;
uint16_t Prom;
uint16_t prom;
uint16_t Max;
uint16_t max;
uint16_t Min;
uint16_t min;
uint16_t j;
uint16_t i;

void Conversor_AD(void);

void funcion_temporizador(void);

void Promedio(void);

void Maximo(void);

void Minimo(void);

void Envia_Maximo(void);

void Envia_Minimo(void);

void Envia_Promedio(void);


//
serial_config mi_conversor={SERIAL_PORT_PC,115200,NULL};
//
timer_config temporizador_para_AD = {TIMER_A,40,&funcion_temporizador};/*Interrupcion para conversion AD para la frecuencia 25hz*/
//
analog_input_config  configuracion_Conversor_AD={CH1,AINPUTS_SINGLE_READ,&Conversor_AD};

   void Inicializacion(void){

	    SystemClockInit();
	    UartInit(&mi_conversor);
	    TimerInit(&temporizador_para_AD);
	   	TimerStart(TIMER_A);
	    AnalogInputInit(&configuracion_Conversor_AD);
	    AnalogOutputInit();
	    SwitchesInit();
	    SwitchActivInt(SWITCH_1,Envia_Promedio);
	   	SwitchActivInt(SWITCH_2,Envia_Maximo);
	   	SwitchActivInt(SWITCH_3,Envia_Minimo);
      }

	  void funcion_temporizador(void){

		  AnalogStartConvertion();
	         }
      void Promedio(void){
    	  j++;
    	  prom=(prom+senial)/j;
    	     }
      void Maximo(void){

          	 if(senial>max){
          		 max=senial;
          	 }
             }
      void Minimo(void){
    	     if(senial<min){
    	        min=senial;
    	     }
             }

      void Envia_Promedio(){

    	  UartSendString(SERIAL_PORT_PC	, UartItoa( Prom, basediez));
    	 		       UartSendString(SERIAL_PORT_PC	, "\r");
             }
      void Envia_Maximo(){

          UartSendString(SERIAL_PORT_PC	, UartItoa( Max, basediez));
         	 		    UartSendString(SERIAL_PORT_PC	, "\r");
             }
      void Envia_Minimo(){

         	  UartSendString(SERIAL_PORT_PC	, UartItoa( Min, basediez));
         	 		       UartSendString(SERIAL_PORT_PC	, "\r");
             }


	  void Conversor_AD(void){
           i++;
		     AnalogInputRead(CH1,&caracter );
		     senial=caracter;
		     Promedio();
		     Maximo();
		     Minimo();

		     if(i=(VALOR25CUENTAS)){         /*25 veces interrumpe el Timer eso es = 1seg*/
                      Max=max;
                      Min=min;
                      Prom=prom;

                      max=0;
                      min=0;
                      prom=0;
                      i=0;
                      j=0;                 /*reinicio el puntero de Promedio()*/

	  }
	  }
 /*===========================================*/

int main(void)
{
	Inicializacion();

    while(1)
    {
        }
}

    


/*==================[end of file]============================================*/
/* */

/*  */

