/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Parcial_Ejercicio_1_Recuperatorio/inc/Parcial_Ejercicio_1.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "timer.h"
#include <UART.h>
#include <stdio.h>
#include <stdint.h>


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
/*Diseñar e implementar el firmware de un sistema de control de objetos en cinta transportadora por infrarrojo.
 *  Empleando el sensor TCRT5000 el sistema debe contar la cantidad de objetos que atraviesan el haz de IR cada 5 segundos.
 *  Si la cantidad de objetos contados está entre 4 y 6, se debe encender el LED 3 y enviar a través de la UART a la PC el mensaje “OK”.
 *
 * Si la cantidad es menor a 4 se debe encender el LED 1 y enviar el mensaje “POCOS”.
 *  Si la cantidad es mayor a 6 se debe encender el LED 2 y enviar el mensaje “MUCHOS”.
 *
 *  (Incluir caracter de fin de línea en los mensajes) .
 *
 *
 */
/*==================[external functions definition]==========================*/

uint8_t led_que_enciende;
uint8_t Contador ;
uint8_t basediez=10;

void Contador_de_objetos(void);
timer_config temporizador_contador_de_objetos = {TIMER_A,5000,&Contador_de_objetos};/*Interrupcion para ENCENDIDO de los LEDs con periodo 5000 ms*/
serial_config mi_puerto={SERIAL_PORT_PC,115200,NULL};

   void Inicializacion(void){

   SystemClockInit();
   LedsInit();
   TimerInit(&temporizador_contador_de_objetos);
   TimerStart(TIMER_A);
   Tcrt5000Init(T_COL0);
      }


     void cambiar_led(void){
    	 Contador++;
       if (4<Contador<6){

	  led_que_enciende=LED_3;
	  UartSendString(SERIAL_PORT_PC	,"Ok");
	  UartSendString(SERIAL_PORT_PC	, "\r\n");
  }
       if(Contador<4){
	  led_que_enciende=LED_1;
	  UartSendString(SERIAL_PORT_PC	,"POCOS");
	  UartSendString(SERIAL_PORT_PC	, "\r\n");
  }
        if(Contador>6){
	  led_que_enciende=LED_2;
	  UartSendString(SERIAL_PORT_PC	,"MUCHOS");
	  UartSendString(SERIAL_PORT_PC	, "\r\n");
  }


}


     void Contador_de_objetos(void){
       LedOn(led_que_enciende);
    }
 /*===========================================*/
int main(void)
{
Inicializacion();
    while(1)
    {
        if (Tcrt5000State()==0){/*detecta si pasa un objeto por delante del IR*/

                cambiar_led();  /*llama a la funcion encargada de contar,comparar,asignar*/

        while(Tcrt5000State()==0){};/*Impide que siga contando si el objeto
                                     permanece delante del IR*/
 }
    }
}
/*==================[end of file]============================================*/

