/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Contador_binario_IR.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
/*Proyecto: Contador de objetos en cinta transportadora
1)	Mostrar cantidad de objetos contados utilizando los leds como un contador binario en el cual el LED_RGB_B (Azul) como el bit 2^3  y el LED_3 como 2^0.
2)	Usar TEC1 para activar y detener el conteo.
3)	Usar TEC2 para mantener el resultado (“HOLD”).
4)	Usar TEC3 para resetear el conteo.
 */
/*==================[external functions definition]==========================*/


uint8_t Contador;
uint8_t retardo=100;
uint8_t estado=1;
uint8_t estadoanterior=0;
bool Hold;
bool estado_tecla;


   void  Cambiar_estado_tecla(uint8_t estado_Tecla){

                             estado_tecla=!estado_Tecla;
                             DelayMs(retardo);
    	       }
   void  Cambiar_estado_Hold(bool hold){

                               Hold=!hold;
                               DelayMs(retardo);
      	       }

   void  Congelar_conteo(uint8_t contador,bool hold){
	                           if (Hold==1){
	                        	   LedsMask(contador);
	  }
	                           else{
	                        	   LedsOffAll();
	  }

}

	void Muestra_conteo_de_IR_en_Binario(uint8_t contador,uint8_t Estado_tecla){

		                      if(Estado_tecla==1){

		                    	  if (contador>8){





		                    		  LedToggle(LED_RGB_R);
		                    		  DelayMs(retardo);
		                    		  DelayMs(retardo);

		                    		  contador=0;
			             }

		                    	  LedsMask(contador);

		 }


					            else{
					            	LedsOffAll();
					            	estado=1;}


	}

 /*===========================================*/


int main(void)
{   uint8_t tecla=0;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(T_COL0);
	/*con Interrupciones
	 *SwitchActivInt(SWITCH_1,Muestra_conteo_de_IR_en_Binario);
	 *SwitchActivInt(SWITCH_2,Muestra_conteo_de_IR_en_Bin);
	 *SwitchActivInt(SWITCH_3,Resetea_el_Contador); */
    while(1)
    {
      estado =Tcrt5000State();

           if(estado==0){
        	   if(estadoanterior==1){

        		 Contador++;

      		}
    }

            estadoanterior=estado;

            	Congelar_conteo(Contador,Hold);
              		DelayMs(retardo);

              	Muestra_conteo_de_IR_en_Binario(Contador,estado_tecla);
              	    DelayMs(retardo);


        	    tecla  = SwitchesRead();


        	    switch(tecla){

        			case(SWITCH_1):/*Activa y detiene el conteo*/

        					Cambiar_estado_tecla(estado_tecla);/*Conmuta el estado de la tecla*/
        						DelayMs(retardo);

        			break;


             	    case(SWITCH_2):/*Congela el ultimo resultado*/

             	    	    Cambiar_estado_Hold(Hold);/*Conmuta Hold*/
             	    	    	 DelayMs(retardo);

        	  	    break;


        	 	     case(SWITCH_3):/*Resetea el conteo*/


       	    			    Contador=0;
        	 	    	    	DelayMs(retardo);
        		     break;



           }

        }


}

    


/*==================[end of file]============================================*/

