/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Parcial_Ejercicio_2_Recuperatorio/inc/Parcial_Ejercicio_2.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "timer.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <UART.h>
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
/*
 * Partiendo del firmware de osciloscopio (proyecto 4),
 * modifique el mismo para que la señal generada en el Conversor Digital Analógico
 * sea una señal dientes de sierra de frecuencia variable entre 0.5Hz y 4Hz
 * y amplitud fija desde 0 a 1V (mantener el código de adquisición con CAD y envío por UART).
 * Se deben poder modificar los parámetros de la señal mediante las teclas:

  *Tecla 1: disminuye frecuencia (- 0.5Hz)
  *Tecla 2: aumenta frecuencia (+ 0.5Hz).
 *
 *
 *
 *
 *
 *
 *
 * */
/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
#define basediez 10

uint16_t caracter;
bool bandera=true;
uint16_t senial;
uint8_t fc;


void Conversor_AD(void);

void funcion_temporizador(void);

void decremento_frecuencia(void);

void Incremento_frecuencia(void);



serial_config mi_conversor={SERIAL_PORT_PC,115200,NULL};
//
timer_config temporizador_para_AD = {TIMER_A,250,&funcion_temporizador};/*Interrupcion para conversion AD para la frecuencia 4hz*/
//
analog_input_config  configuracion_Conversor_AD={CH1,AINPUTS_SINGLE_READ,&Conversor_AD};

   void Inicializacion(void){

	    SystemClockInit();
	    UartInit(&mi_conversor);
	    TimerInit(&temporizador_para_AD);
	   	TimerStart(TIMER_A);
	    AnalogInputInit(&configuracion_Conversor_AD);
	    AnalogOutputInit();
	    SwitchesInit();
	    SwitchActivInt(SWITCH_1,decremento_frecuencia);
	   	SwitchActivInt(SWITCH_2,Incremento_frecuencia);
      }

      void  Cambio_de_bandera(){

  	  	  bandera=!bandera;
  	  }

	  void funcion_temporizador(void){

		  AnalogStartConvertion();


	  }

      void decremento_frecuencia(void){
           fc-0.5;

      }

      void Incremento_frecuencia(void){
    	   fc+0.5;

      }

	  void Conversor_AD(void){

        static uint8_t  i;

		 AnalogInputRead(CH1,&caracter );/*Lee el Canal_1 de placa y lo asigna a variable caracter*/
		     senial=caracter;/**/

		       UartSendString(SERIAL_PORT_PC	, UartItoa( senial, basediez));            /*saco señal filtrada por puerto serie*/
		       UartSendString(SERIAL_PORT_PC	, "\r");



		    	 AnalogOutputWrite(i);/*saca por el puerto  una "señal_diente_de_sierra"
		    	                        que cada punto es mayor en amplitud que el anterior*/

	            i++;

			      if(i>300 ){     /*la  cantidad de pulsos en amplitud es menor a 300 aprox. a 1V*/

				     i=0;



		  }

	  }



 /*===========================================*/


int main(void)
{


	Inicializacion();


    while(1)
    {


        }


}

    


/*==================[end of file]============================================*/


