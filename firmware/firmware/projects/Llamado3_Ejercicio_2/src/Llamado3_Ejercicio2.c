/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "timer.h"
#include <UART.h>
#include "../inc/Llamado3_Ejercicio2.h"       /* <= own header */
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
/*Se pretende diseñar un goniómetro digital de rodilla utilizando un potenciómetro solidario al eje de dos piezas móviles (ver imagen).
 *Para esto se debe digitalizar la señal del punto medio del potenciómetro y calcular el ángulo, sabiendo a 0º la tensión de salida es 0V y a 180º es 3.3V.
 *El sistema deberá además enviar a través de la UART tanto el valor de ángulo actual como el valor máximo medido hasta el momento (“XXº - YYº\r\n” donde XX es el valor de ángulo actual y YY el valor máximo de ángulo medido hasta ese instante).
 *
 *  Para el control del sistema se deben usar las teclas:
Tecla 1: Encendido.
Tecla 2: Apagado.
Tecla 3: Reseteo de la medición.
 *
 *
 *
 *
 *

 *
 *
 *
 *
 *
 *
 *
 * */
/*==================[internal data definition]===============================*/
#define VALOR25CUENTAS 25

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
#define basediez 10

uint16_t Tension_potenciometro;
uint16_t  Tension;
float  Grados_de_Movimiento;
uint16_t Grados_Mov;
bool On=1;
uint16_t max;


void Conversor_AD(void);

void funcion_temporizador(void);

void Reseteo_de_cuenta(void);
void Encendido(void);
void Apagado(void);

//
serial_config mi_conversor={SERIAL_PORT_PC,115200,NULL};
//
timer_config temporizador_para_AD = {TIMER_A,10,&funcion_temporizador};/*Interrupcion para conversion AD cada 10 mseg*/
//
analog_input_config  configuracion_Conversor_AD={CH1,AINPUTS_SINGLE_READ,&Conversor_AD};

   void Inicializacion(void){

	    SystemClockInit();
	    UartInit(&mi_conversor);
	    TimerInit(&temporizador_para_AD);
	   	TimerStart(TIMER_A);
	    AnalogInputInit(&configuracion_Conversor_AD);
	    AnalogOutputInit();
	    SwitchesInit();
	    SwitchActivInt(SWITCH_1,Encendido);
	    SwitchActivInt(SWITCH_2,Apagado);
	    SwitchActivInt(SWITCH_3,Reseteo_de_cuenta);
      }

	  void funcion_temporizador(void){

      if(On==1){                          /*Comienza solo si la bandera On==true*/
		  AnalogStartConvertion();
	         }}


	  void Envia_Valor_Actual_y_Maximo(void){


		  if(Grados_Mov>max){
		           		 max=Grados_Mov;
		           	 }
		     UartSendString(SERIAL_PORT_PC	, "Valor agulo actual - Maximo:");
				                      UartSendString(SERIAL_PORT_PC	, UartItoa(Grados_Mov, basediez));
				                      UartSendString(SERIAL_PORT_PC	, "-");
				                      UartSendString(SERIAL_PORT_PC	, UartItoa(max, basediez));
				                      UartSendString(SERIAL_PORT_PC	, "\r\n");
	  };


	    void Reseteo_de_cuenta(void){

	    	Grados_Mov=0;
	    	max=0;
	    }
	    void Encendido(void){

	    	On=1;

	    }
	    void Apagado(void){
	    	On=0;

	    }





	  void Conversor_AD(void){

		     AnalogInputRead(CH1,&Tension_potenciometro );
		     Tension=Tension_potenciometro;
		     Grados_de_Movimiento=(180*Tension)/3.3;/*max en 180º es 3.3V valor ingresado * 180º/3.3V*/
		     Grados_Mov=(int) Grados_de_Movimiento;



	  }

 /*===========================================*/

int main(void)
{
	Inicializacion();

    while(1)
    {
        }
}

    


/*==================[end of file]============================================*/
/* */

/*  */

