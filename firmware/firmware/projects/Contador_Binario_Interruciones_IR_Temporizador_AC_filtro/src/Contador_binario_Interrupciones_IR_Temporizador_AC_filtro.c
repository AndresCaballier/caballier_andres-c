/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "timer.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <UART.h>
#include "../inc/Contador_binario_Interrupciones_IR_Temporizador_AC_filtro.h"       /* <= own header */
#include "analog_io.h"




/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
#define basediez 10
#define BUFFER_SIZE 256


const char ecg[] = {
17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
17,17,17

} ;


uint16_t caracter;
bool bandera=true;
uint16_t senial_filtrada_anterior;
uint16_t senial_filtrada;
uint16_t filtro_on=0;

float alfa;
uint8_t fc;
float Rc;
float dt=0.002;




void Conversor_AD(void);

void funcion_temporizador(void);

void filtro_activo(void);

void filtro_inactivo(void);

void decremento_frecuencia(void);

void Incremento_frecuencia(void);



serial_config mi_conversor={SERIAL_PORT_PC,115200,NULL};/*La velocidad me parece suficientes  bytes/seg */
//
timer_config temporizador_para_AD = {TIMER_A,2,&funcion_temporizador};/*Interrupcion para conversion AD*/
//
analog_input_config  configuracion_Conversor_AD={CH1,AINPUTS_SINGLE_READ,&Conversor_AD};

   void Inicializacion(void){

	    SystemClockInit();
	    UartInit(&mi_conversor);
	    TimerInit(&temporizador_para_AD);
	   	TimerStart(TIMER_A);
	    AnalogInputInit(&configuracion_Conversor_AD);
	    AnalogOutputInit();
	    SwitchesInit();
	    SwitchActivInt(SWITCH_1,filtro_activo);
	   	SwitchActivInt(SWITCH_2,filtro_inactivo);
	    SwitchActivInt(SWITCH_3,decremento_frecuencia);
	   	SwitchActivInt(SWITCH_4,Incremento_frecuencia);
      }

      void  Cambio_de_bandera(){

  	  	  bandera=!bandera;
  	  }

	  void funcion_temporizador(void){

		  AnalogStartConvertion();


	  }

	  void filtro_activo(void){

		  filtro_on=1;
	  }

      void filtro_inactivo(void){

    	  filtro_on=0;
      }

      void decremento_frecuencia(void){
           fc--;

      }

      void Incremento_frecuencia(void){
    	   fc++;

      }

	  void Conversor_AD(void){

        static uint8_t  i=0;              /*Puntero para recorrer el la señal de ecg[i]*/

		 AnalogInputRead(CH1,&caracter );/*Lee el Canal_1 de placa y lo asigna a variable caracter*/

          if(filtro_on){

           Rc=1/(fc*2*3.14);               /*constante de tiempo
                                            para implementar el filtro pasabajos*/
           alfa=dt/(Rc+dt);                /*alfa del filtro pasabajos*/

		    senial_filtrada_anterior= senial_filtrada;

		     senial_filtrada=senial_filtrada_anterior + alfa * (caracter - senial_filtrada_anterior);/*Filtro pasabajos*/

		       UartSendString(SERIAL_PORT_PC	, UartItoa(caracter, basediez));                    /*saco por puerto serie señal original*/
		       UartSendString(SERIAL_PORT_PC	, ",");
		       UartSendString(SERIAL_PORT_PC	, UartItoa( senial_filtrada, basediez));            /*saco señal filtrada por puerto serie*/
		       UartSendString(SERIAL_PORT_PC	, "\r");

		  }



		  Cambio_de_bandera();            /*Activa o desactiva bandera*/

		     if(bandera==0){

			    AnalogOutputWrite(ecg[i]);/*saca por el puerto el ecg[i]*/

	            i++;

			      if(i>BUFFER_SIZE ){     /*resetea el puntero, i=0*/

				     i=0;



		  }
		}
	  }



 /*===========================================*/


int main(void)
{


	Inicializacion();


    while(1)
    {


        }


}

    


/*==================[end of file]============================================*/

