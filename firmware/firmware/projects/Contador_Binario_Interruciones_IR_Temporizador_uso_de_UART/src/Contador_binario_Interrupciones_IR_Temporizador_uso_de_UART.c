/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "timer.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <UART.h>


#include "../inc/Contador_binario_Interrupciones_IR_Temporizador_uso_UART.h"       /* <= own header */

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
/*Proyecto: Contador de objetos en cinta transportadora
1)	Mostrar cantidad de objetos contados utilizando los leds como un contador binario en el cual el LED_RGB_B (Azul) como el bit 2^3  y el LED_3 como 2^0.
2)	Usar TEC1 para activar y detener el conteo.
3)	Usar TEC2 para mantener el resultado (“HOLD”).
4)	Usar TEC3 para resetear el conteo.
 */
/*==================[external functions definition]==========================*/
#define basediez 10

uint8_t Contador;
uint8_t retardo=100;
uint8_t estado=1;
uint8_t estadoanterior=0;
bool Hold;
bool estado_tecla;
bool reset;


void Muestra_conteo_de_IR_en_Binario(void);

void Deteccion_Caracter_Entrada(void);


timer_config mi_temporizador = {TIMER_A,1000,&Muestra_conteo_de_IR_en_Binario};

serial_config mi_puerto={SERIAL_PORT_PC,115200,Deteccion_Caracter_Entrada};


   void Inicializacion_SystemClock_Leds_Switches_Tcrt5000_Timer(void){

	    SystemClockInit();
	   	LedsInit();
	   	SwitchesInit();
	   	Tcrt5000Init(T_COL0);
	   	TimerInit(&mi_temporizador);
	   	TimerStart(TIMER_A);

   }







   void  Cambiar_estado_tecla(){

                               estado_tecla=!estado_tecla;

    	       }
   void  Cambiar_estado_Hold(){

                               Hold=!Hold;

      	       }
   void  Cambiar_estado_reset(){

                               reset=!reset;

      	       }

   void  Congelar_conteo(uint8_t contador,bool hold){
	                           if (hold==1){
	                        	   LedsMask(contador);
	  }
	                           else{
	                        	   LedsOffAll();
	  }

}
   void Resetea_el_Contador(bool Reset ){

	                           if (Reset==1){
	                        	   Contador=0;

	                        	   DelayMs(retardo);
	                           }



   }



	void Muestra_conteo_de_IR_en_Binario(void){

		                      if(estado_tecla==1){

		                    	  if (Contador>8){

		                    		  LedToggle(LED_RGB_R);
		                    		  DelayMs(retardo);
		                    		  DelayMs(retardo);

		                    		  Contador=0;
			             }

		                    	  LedsMask(Contador);

		 }


					            else{
					            	LedsOffAll();
					            	estado=1;}

		                      UartSendString(SERIAL_PORT_PC	,"Cantidad:");
		                      UartSendString(SERIAL_PORT_PC	, UartItoa(Contador, basediez));
		                      UartSendString(SERIAL_PORT_PC	, "\r\n");


	}


	  void Deteccion_Caracter_Entrada(void){

	   uint8_t Dato;

		UartReadByte(SERIAL_PORT_PC, &Dato);


		if (Dato=="H"){

			 Hold=!Hold;


		}
		    if (Dato=="O"){

		    	 estado_tecla=!estado_tecla;

			}

		        if (Dato=="0"){

		    	    reset=!reset;

		    	}

	}

 /*===========================================*/


int main(void)
{


	Inicializacion_SystemClock_Leds_Switches_Tcrt5000_Timer();


     UartInit(&mi_puerto);

	 SwitchActivInt(SWITCH_1,Cambiar_estado_tecla);
	 SwitchActivInt(SWITCH_2,Cambiar_estado_Hold);
	 SwitchActivInt(SWITCH_3,Cambiar_estado_reset);

    while(1)
    {
      estado =Tcrt5000State();

           if(estado==0){
        	   if(estadoanterior==1){

        		 Contador++;

      		}
    }

            estadoanterior=estado;

            	Congelar_conteo(Contador,Hold);
              		DelayMs(retardo);

              	Resetea_el_Contador(reset);
              	    DelayMs(retardo);



        }


}

    


/*==================[end of file]============================================*/

