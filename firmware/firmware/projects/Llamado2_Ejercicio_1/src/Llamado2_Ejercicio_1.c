/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Diseñar e implementar el firmware de un sistema de medición de velocidad por infrarrojo.
 * Empleando el sensor TCRT5000 el sistema debe contar la cantidad de pulsos generados por un disco solidario a una rueda, el cual posee 20 aberturas (ver esquema)
 * Si la velocidad medida se encuentra entre 40 y 60 rpm, se debe encender el LED 3 y enviar a través de la UART a la PC el mensaje “OK”.
 * Si la cantidad es menor a 40 rpm se debe encender el LED 1 y enviar el mensaje “SLOW”.
 * Si la cantidad es mayor a 60 se debe encender el LED 2 y enviar el mensaje “FAST”.
 *(Incluir caracter de fin de línea en los mensajes) .*/

/*==================[inclusions]=============================================*/
#include "Llamado2_Ejercicio_1.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "timer.h"
#include <UART.h>
#include <stdio.h>/*innesesario*/
#include <stdint.h>/*esta demas*/

/*==================[macros and definitions]=================================*/
#define VALOR60SEG 60
/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
/*
 */
/*==================[external functions definition]==========================*/

uint8_t led_que_enciende;
uint8_t Contador ;
uint8_t basediez=10;
uint8_t i;

void Contador_de_pulsos(void);
//
timer_config temporizador_contador_de_revoluciones = {TIMER_A,1000,&Contador_de_pulsos};
serial_config mi_puerto={SERIAL_PORT_PC,115200,NULL};

   void Inicializacion(void){

   SystemClockInit();
   LedsInit();
   UartInit(&mi_puerto);
   TimerInit(&temporizador_contador_de_revoluciones);
   TimerStart(TIMER_A);
   Tcrt5000Init(T_COL0);
      }


     void Conteo(void){
    	 Contador++;
     }


     void Contador_de_pulsos(void){
    	 i++;
    	 if(i=VALOR60SEG){

    		       if ((Contador >= 13) && (Contador <= 20)){/*40rev*20/60=13; 60rev*20/60=20;60seg=1min*/

    			  led_que_enciende=LED_3;
    			  UartSendString(SERIAL_PORT_PC	,"Ok");
    			  UartSendString(SERIAL_PORT_PC	, "\r\n");
    		  }
    		       if(Contador<13){
    			  led_que_enciende=LED_1;
    			  UartSendString(SERIAL_PORT_PC	,"SLOW");
    			  UartSendString(SERIAL_PORT_PC	, "\r\n");
    		  }
    		        if(Contador>20){
    			  led_que_enciende=LED_2;
    			  UartSendString(SERIAL_PORT_PC	,"FAST");
    			  UartSendString(SERIAL_PORT_PC	, "\r\n");
    		  }
       i=0;
       Contador=0;
    	 }
       LedOn(led_que_enciende);

    }
 /*===========================================*/
int main(void)
{
Inicializacion();
    while(1)
    {
        if (Tcrt5000State()==0){          /*detecta si pasa un objeto por delante del IR*/

        	Conteo();                     /*llama a la funcion encargada de contar*/

        while(Tcrt5000State()==0){};      /*Impide que siga contando si el objeto
                                           permanece delante del IR*/
 }
    }
}
/*==================[end of file]============================================*/

