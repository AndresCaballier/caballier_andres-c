/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Diseñar e implementar el firmware de un odómetro (medidor de distancia).
 * El mismo está implementado usando un sensor TCRT5000 que debe contar la cantidad de pulsos generados por un disco perforado solidario a una rueda.
 * El disco posee 20 aberturas y la rueda 32cm de diámetro.
 * En funcionamiento, el sistema debe ir acumulando la distancia recorrida, e informarla a través de la UART cada un segundo, con el formato “XX cm\r\n”.
 * Para el control del sistema se deben usar las teclas:
Tecla 1: Encendido.
Tecla 2: Apagado.
Tecla 3: Reseteo de cuenta.
 *
 *
 *
 * Diseñar e implementar el firmware de un sistema de medición de velocidad por infrarrojo.
 * Empleando el sensor TCRT5000 el sistema debe contar la cantidad de pulsos generados por un disco solidario a una rueda, el cual posee 20 aberturas (ver esquema)
 * Si la velocidad medida se encuentra entre 40 y 60 rpm, se debe encender el LED 3 y enviar a través de la UART a la PC el mensaje “OK”.
 * Si la cantidad es menor a 40 rpm se debe encender el LED 1 y enviar el mensaje “SLOW”.
 * Si la cantidad es mayor a 60 se debe encender el LED 2 y enviar el mensaje “FAST”.
 *(Incluir caracter de fin de línea en los mensajes) .*/

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "timer.h"
#include <UART.h>
#include "../inc/LLamado3_Ejercicio_1.h"       /* <= own header */

/*==================[macros and definitions]=================================*/
#define PERIMETRO 1/*surge de realizar la cuenta*/
/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
/*
 */
/*==================[external functions definition]==========================*/

uint8_t Distancia;
uint8_t Contador ;
bool encendido=true;
uint8_t basediez=10;


void Envia_Distacia_Puertoserie(void);
void receteo_de_cuenta(void);
void encendido(void);
void off(void);
//
timer_config temporizador_Envia_Distacia_Puertoserie = {TIMER_A,1000,& Envia_Distacia_Puertoserie};
serial_config mi_puerto={SERIAL_PORT_PC,115200,NULL};

   void Inicializacion(void){

   SystemClockInit();
   UartInit(&mi_puerto);
   TimerInit(&temporizador_Envia_Distacia_Puertoserie);
   TimerStart(TIMER_A);
   Tcrt5000Init(T_COL0);
   SwitchActivInt(SWITCH_1,encendido);
   SwitchActivInt(SWITCH_2,off);
   SwitchActivInt(SWITCH_3,receteo_de_cuenta);
      }


     void Acumulador_de_Distancia(void){
    	 Contador++;
    	 if(Contador=20){
    	 Distancia=Distancia+Perimetro;/*Perimetro=2*Pi*radio=1metro*/
    	 }
     }


     void  Envia_Distacia_Puertoserie(void){


    	 UartSendString(SERIAL_PORT_PC	, UartItoa(Distancia, basediez));
    	 UartSendString(SERIAL_PORT_PC	, "\r\n");


    }

    void Receteo_de_cuenta(void){

    	Contador=0;
    	Distancia=0;
    }
    void Encendido(void){

    	encendido=1;

    }
    void off(void){
    	encendido=0;

    }
 /*===========================================*/
int main(void)
{
Inicializacion();
    while(1)
    {
    if (encendido==1){

        if (Tcrt5000State()==0){          /*detecta si pasa un objeto por delante del IR*/

        	Acumulador_de_Distancia();    /*llama a la funcion encargada de Acumular Distancia*/

        while(Tcrt5000State()==0){};      /*Impide que siga contando si el objeto
                                           permanece delante del IR*/
     }
        }
        if(encendido==0){
        	/*no hace nada*/
        }
    }
}
/*==================[end of file]============================================*/

