/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *

D) Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo
gpioConf_t.
typedef struct
{
uint8_t port; !< GPIO port number
uint8_t pin; !< GPIO pin number
 uint8_t dir; !< GPIO direction ‘0’ IN; ‘1’ OUT
} gpioConf_t;
Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14*/
/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[end of file]============================================*/


/*==================[internal functions declaration]=========================*/
typedef struct
{
uint8_t port;
uint8_t pin;
uint8_t dir;
} gpioConf_t;

uint8_t i=0;//Contador del vector de struct.
uint8_t BCD;




void Configuracion_de_puertos(uint8_t n_BCD,gpioConf_t *B){

	BCD=n_BCD;

	for(i=0;i<4;i++){

		 printf(" El puerto:%d,%d \n",B[i].port,B[i].pin);
		   if(n_BCD%2==1){
			   printf("Esta ON \n");

		   }

		     else{
		    	 printf("Esta OFF \n");
		     }
		        n_BCD=n_BCD/2;

	}


	   printf("Configuracion_de_puertos  \n");


uint8_t v1[4]={0,0,0,1};
uint8_t v2[4]={0,0,1,0};
uint8_t v3[4]={0,0,1,1};
uint8_t v4[4]={0,1,0,0};
uint8_t v5[4]={0,1,0,1};
uint8_t v6[4]={0,1,1,0};
uint8_t v7[4]={0,1,1,1};
uint8_t v8[4]={1,0,0,0};
uint8_t v9[4]={1,0,0,1};


	switch(BCD){

	case 0:
		for(i=0;i<4;i++){
		                               B[i].dir=0;
		                             printf(" Esta encendido el 0,la salida en los pines es:%d\r\n",B[i].dir);
		                 }
			break;

	case 1:
		for(i=0;i<4;i++){
				                        B[i].dir=v1[i];
				                       printf(" Esta encendido el 1,la salida en los pines es:%d\r\n",B[i].dir);
		}
		    break;

	case 2:
		for(i=0;i<4;i++){
						                B[i].dir=v2[i];
						                printf(" Esta encendido el 2,la salida en los pines es:%d\r\n",B[i].dir);
						}
			break;

	case 3:
		for(i=0;i<4;i++){
						                B[i].dir=v3[i];
						               printf(" Esta encendido el 3,la salida en los pines es:%d\r\n",B[i].dir);
		                }
						               break;


	case 4:
		for(i=0;i<4;i++){
						                B[i].dir=v4[i];
						               printf(" Esta encendido el 4,la salida en los pines es:%d\r\n",B[i].dir);
		                }
			break;

	case 5:
		for(i=0;i<4;i++){
						                B[i].dir=v5[i];
						               printf(" Esta encendido el 5,la salida en los pines es:%d\r\n",B[i].dir);
		                }
			break;

	case 6:
		for(i=0;i<4;i++){
						                B[i].dir=v6[i];
						               printf(" Esta encendido el 6,la salida en los pines es:%d\r\n",B[i].dir);

		                 }
			break;

	case 7:
		for(i=0;i<4;i++){
						                B[i].dir=v7[i];
						               printf(" Esta encendido el 7,la salida en los pines es:%d\r\n",B[i].dir);
		                 }
			break;

	case 8:
		for(i=0;i<4;i++){
						                B[i].dir=v8[i];
						               printf(" Esta encendido el 8,la salida en los pines es:%d\r\n",B[i].dir);
		                 }
			break;

	case 9:
		for(i=0;i<4;i++){
						                B[i].dir=v9[i];
						                printf(" Esta encendido el 9,la salida en los pines es:%d\r\n",B[i].dir);
						}

			break;
	}

}
int main(void)
{
	 gpioConf_t b[4];//b0,b1,b2,b3

	uint8_t bcd;
		             bcd=9;
		                     //Configuracion de puertos
				b[0].port=1;
				b[1].port=1;
				b[2].port=1;
				b[3].port=2;

				b[0].pin=4;//configuracion de pines
				b[1].pin=5;
				b[2].pin=6;
				b[3].pin=14;

				b[0].dir=1;//todos los pines son salida; dir=1
			    b[1].dir=1;
				b[2].dir=1;
				b[3].dir=1;


	Configuracion_de_puertos( bcd, b);/*El nombre
	                                        del vector es le puntero al primer elemento*/

return 0;

}

/*==================[end of file]============================================*/

