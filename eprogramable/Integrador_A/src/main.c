/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/
struct Leds{
	uint8_t numero_de_led;
	uint8_t numero_de_ciclos;
	uint8_t periodo;
    uint8_t modo ;

};
struct Leds *Luz;

const uint8_t on=1,off=2,toogle=3;

/*==================[internal functions declaration]=========================*/
void operar(struct Leds *led){

	uint8_t num_led=led->numero_de_led;
	uint8_t num_ciclos=led->numero_de_ciclos;
	uint8_t period=led->periodo;
	uint8_t mod=led->modo;
	uint8_t i=0;                  //contador de numero de ciclos.
	uint8_t j=0;                  //contador de periodo.

			//char estado;
			if (mod==on){
			switch ( num_led){

			case 1: printf ("%d esta encendido \r\n",num_led);

			        break;

			case 2: printf ("%d esta encendido \r\n",num_led);

					break;
			case 3: printf("%d esta encendido \r\n",num_led);

					break;
			}};

			     if(mod==off){
				       switch ( num_led){

			case 1: printf ("%d está apagado \r\n",num_led);

			        break;

			case 2: printf ("%d está apagado \r\n",num_led);

					break;
			case 3: printf ("%d está apagado \r\n",num_led);

					break;}}

			     if(mod==toogle){//modo Toogle, parpadeo;

			    	while (i<=num_ciclos){

                                for( j=0;j<period;j++){

                                       printf("%d esperando:\r\n",j);};


                    switch ( num_led){
                        case 1:
			  			printf("%d está titilando \r\n",num_led);
                             break;

                        case 2:
			  			    printf ("%d está titilando \r\n",num_led);
                                 break;

			  		    case 3:
			  			        printf ("%d está titilando \r\n",num_led);
                                     break;}

                       i++;






			     }
			            }

                         else{printf("Fin\n");}


                                 }


		void enciende_Led(uint8_t n_led){
			Luz->numero_de_led=n_led;
			         printf("%d enciende  \r\n", Luz->numero_de_led);
		};
		void apaga_Led(uint8_t n_led){
			Luz->numero_de_led=n_led;
					 printf("%d apaga  \r\n", Luz->numero_de_led);
		};
		void titila_Led(uint8_t n_led){
				Luz->numero_de_led=n_led;
					 printf("%d titila  \r\n", Luz->numero_de_led);
			};




int main(void)

{struct Leds Led;

Led.modo=1;//off=2,on=1,toogle=3
Led.numero_de_led=2;
Led.numero_de_ciclos=10;
Led.periodo=100;
operar(&Led);
return 0;
}






/*==================[end of file]============================================*/

