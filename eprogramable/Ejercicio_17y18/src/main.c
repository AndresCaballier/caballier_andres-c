/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Ejercicio_17y18/inc/main.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/



 uint16_t Acumulador=0;
 uint8_t i;
 uint8_t Promedio;
 uint8_t Promedioc;

/*==================[internal functions declaration]=========================*/


uint16_t Sumatoria(uint8_t Cantidad,uint8_t *Num){


	    for( i=0;i<Cantidad;i++){

		     Acumulador+=Num[i];


	}





               return Acumulador;


}
int main(void)
{uint8_t Num_Cantidad = 16;
uint16_t A;/*sumatoria de elementos del arreglo*/
uint8_t  Numeros[16]={234 ,123 ,111 ,101 ,32,116 ,211 ,24 ,214 ,100,124 ,222 ,1 ,129 ,9,233};

 A= Sumatoria( Num_Cantidad,Numeros);

 Promedio=A>>4;
  Promedioc=A/Num_Cantidad;

   printf("promedio con corrimiento %d\n", Promedio);

     printf("Promedio comun:%d",Promedioc);

return 0;
}

/*==================[end of file]============================================*/

