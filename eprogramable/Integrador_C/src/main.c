/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Integrador_C/inc/main.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/
/*C) Escriba una función que reciba un dato de 32 bits, la cantidad de dígitos de salida y un puntero a
un arreglo donde se almacene los n dígitos. La función deberá convertir el dato recibido a BCD,
guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.
int8_t BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
}*/

/*==================[internal functions declaration]=========================*/

//uint32_t bcd_numero[];
uint8_t centena_dato;
uint8_t decena_dato;
uint8_t unidad_dato;
uint8_t i;//contador del ciclo for para cargar el arreglo con el numero

void Binario_a_BCD(uint32_t Dato,uint8_t digito,uint8_t *bcd_numero){
//convercion de Dato a BCD

uint32_t dato=Dato;
uint8_t auxiliar_dato;


//bcd_numero[i]= resto de la division entera y luego divide 10 en el bucle


for(i=0; i<digito; i++){
	auxiliar_dato=dato%10;
		bcd_numero[i]=auxiliar_dato;
		  dato=dato/10;
		}


//digito=  cantidad de digitos de salida


}

int main(void)
{uint8_t BCD_num[4];
 uint32_t numeros=1567;
 uint8_t digitos=4;

Binario_a_BCD(numeros,digitos,BCD_num);

printf("%d digito 1\r\n",BCD_num[0]);
printf("%d digito 2\r\n",BCD_num[1]);
printf("%d digito 3\r\n",BCD_num[2]);
printf("%d digito 4\r\n",BCD_num[3]);

return 0;
}






/*==================[end of file]============================================*/

